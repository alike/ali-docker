#!/bin/bash

# ALICE
alias ali='source /cvmfs/alice.cern.ch/etc/login.sh && alienv setenv VO_ALICE@AliPhysics,CMake/v2.8.12-2 -c /bin/bash'
[ -z $ALIPHYSICS_VERSION ] || { PS1="($ALIPHYSICS_VERSION) $PS1";}

# EOS ALIKE
export EOS_MGM_URL="root://alieos.saske.sk"