#!/bin/bash

# $ cd <root dir>
# $ scripts/build.sh

DOCKER_CMD=${DOCKER_CMD-"sudo docker"}
DOCKER_CLEAN_OPT=${DOCKER_CLEAN_OPT-""}
DOCKER_PUSH_HOST=${DOCKER_PUSH_HOST-"docker.io"}
DOCKER_PUSH_GROUP=${DOCKER_PUSH_GROUP-"mvala"}

DOCKER_BUILD_IMAGES="ali"

function help() {
    echo "" >&2
    echo "Usage: $0 {|clean} {all|<docker-image-name>}" >&2
    echo "  all :  $DOCKER_BUILD_IMAGES" >&2
    echo "" >&2
    exit 1
}

[ "$1" == "clean" ] && { DOCKER_CLEAN_OPT="--no-cache=true"; shift; }

[ -z "$1" ] && help

[ "$1" != "all" ] && { DOCKER_BUILD_IMAGES="$*"; }

echo $DOCKER_BUILD_IMAGES

for DOCKER_IMAGE_NAME in $DOCKER_BUILD_IMAGES; do
    $DOCKER_CMD build $DOCKER_CLEAN_OPT -t $DOCKER_PUSH_GROUP/$DOCKER_IMAGE_NAME -f Dockerfile.$DOCKER_IMAGE_NAME .
    $DOCKER_CMD tag $DOCKER_PUSH_GROUP/$DOCKER_IMAGE_NAME $DOCKER_PUSH_HOST/$DOCKER_PUSH_GROUP/$DOCKER_IMAGE_NAME
    $DOCKER_CMD push $DOCKER_PUSH_HOST/$DOCKER_PUSH_GROUP/$DOCKER_IMAGE_NAME || { echo "Error pushing to $DOCKER_PUSH_HOST !!!"; exit 1;}
done
