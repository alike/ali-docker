# ALIKE cvmfs and docker

## Install cvmfs

### RHEL/CENTOS 7
Install epel repository
```
yum install epel-release
```
Install CVMFS repository
```
yum install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
```
Installation of needed packages
```
yum install cvmfs cvmfs-config-default
```

### Fedora 27
Install CVMFS repository
```
dnf copr enable mvala/ob
```
Installation of needed packages
```
dnf install cvmfs cvmfs-config-default
```

### OSX 
You have to install FUSE first. Go to: https://osxfuse.github.io and download and install (.dmg) latest version of FUSE for macOS (necessary for cmvfs). 
Now one can install cvmfs. To do this, download package from [here](https://ecsft.cern.ch/dist/cvmfs/cvmfs-2.4.4/cvmfs-2.4.4.pkg) 
and download the .pkg file. In case of the problems with opening, try to open with RIGHT mouse click (and open) because it is not oficially suported software.


## Configure CVMFS config
```
cat <<EOF >  /etc/cvmfs/default.local
CVMFS_REPOSITORIES=alice.cern.ch
CVMFS_HTTP_PROXY="DIRECT"
CVMFS_QUOTA_LIMIT=10000
EOF
```
Configure cvmfs (only for first time)
```
cvmfs_config setup
```
Testing if everything is OK

```
[root@vala ~]# cvmfs_config probe
Probing /cvmfs/alice.cern.ch... OK
```
If the testing failed, make sure you have mounted alice.cern.ch directory:

```
sudo mount -t cvmfs alice.cern.ch /cvmfs/alice.cern.ch
```

Let's list directory in cvmfs (you may feel little bit of hang for few seconds)
```
[mvala@vala ~]$ ls -al /cvmfs/alice.cern.ch
total 29K
drwxr-xr-x. 14 cvmfs cvmfs 4.0K Nov 27  2012 .
drwxrwxr-x.  2 cvmfs cvmfs    3 Nov  2  2016 bin
lrwxrwxrwx.  1 cvmfs cvmfs   37 Mar 19  2014 calibration -> /cvmfs/alice-ocdb.cern.ch/calibration
-rw-r--r--.  1 cvmfs cvmfs 3.3K Oct 20 13:37 .cvmfsdirtab
-rw-r--r--.  1 cvmfs cvmfs 1.9K Dec 10  2014 .cvmfsdirtab~
drwxr-xr-x.  4 cvmfs cvmfs    3 Dec  8 17:01 data
lrwxrwxrwx.  1 cvmfs cvmfs   20 Mar  9  2016 el5-x86_64 -> x86_64-2.6-gnu-4.1.2
drwxrwxr-x.  4 cvmfs cvmfs    4 Mar 10  2016 el6-x86_64
drwxrwxr-x.  4 cvmfs cvmfs    4 Mar 10  2016 el7-x86_64
drwxr-xr-x.  3 cvmfs cvmfs    3 Mar  9  2016 etc
drwxr-xr-x.  4 cvmfs cvmfs    3 Apr 26  2016 ubuntu1404-x86_64
drwxr-xr-x.  4 cvmfs cvmfs    4 Nov  2  2016 ubuntu1604-x86_64
drwxrwxr-x.  2 cvmfs cvmfs    3 Mar  3 02:22 .utils
drwxr-xr-x.  4 cvmfs cvmfs    4 Dec 20  2012 x86_64-2.6-gnu-4.1.2
drwxrwxr-x.  4 cvmfs cvmfs 4.0K Dec  5  2013 x86_64-2.6-gnu-4.7.2
drwxr-xr-x.  4 cvmfs cvmfs 4.0K Sep 18  2014 x86_64-2.6-gnu-4.8.3
drwxr-xr-x.  4 cvmfs cvmfs 4.0K Mar 18  2015 x86_64-2.6-gnu-4.8.4
```

## Running ROOT/AliROOT from cvmfs

> Note that only centos 7 or sl7 on host should work propertly

```
[mvala@vala ~]$ source /cvmfs/alice.cern.ch/etc/login.sh
[mvala@vala ~]$ alienv setenv VO_ALICE@AliPhysics,CMake/v2.8.12-2 -c /bin/bash
(vAN-20180304-1) [mvala@vala ~]$ aliroot
  *******************************************
  *                                         *
  *        W E L C O M E  to  R O O T       *
  *                                         *
  *   Version   5.34/30     23 April 2015   *
  *                                         *
  *  You are welcome to visit our Web site  *
  *          http://root.cern.ch            *
  *                                         *
  *******************************************

ROOT 5.34/30 (heads/v5-34-00-patches@v5-34-28-57-gec27989, Feb 14 2018, 16:48:00 on linuxx8664gcc)

CINT/ROOT C/C++ Interpreter version 5.18.00, July 2, 2010
Type ? for help. Commands must be C++ statements.
Enclose multiple statements between { }.
root [0] .q
(vAN-20180304-1) [mvala@vala ~]$ 
```
### Running ROOT from cvmfs for OSX
While aliroot using cvmfs is not available for macs, the ROOT is. Firstly create a directory sft.cern.ch:
```
sudo mkdir -p /cvmfs/sft.cern.ch
```
and then mount it:
```
sudo mount -t cvmfs sft.cern.ch /cvmfs/sft.cern.ch
```
The standalone installations of ROOT for MacOSX 10.12. (or MacOSX 10.13.) are localized at:
```
/cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.12.06/x86_64-mac1012-clang90-opt
/cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.12.06/x86_64-mac1013-clang90-opt
```
Another option (not using cvmfs) is just downloading and installing .dmg file from https://root.cern.ch/downloading-root . The root will be installed in `/Applications` folder as separate 
folder like for example ``root_v6.12.06``. 


## Running via DOCKER

### Setup docker
```
yum install -y docker
systemctl start docker
systemctl enable docker
```
Then follow instruction [here](https://www.projectatomic.io/blog/2015/08/why-we-dont-let-non-root-users-run-docker-in-centos-fedora-or-rhel/)

### Run ALI docker
Download `ali-docker` script
```
curl https://gitlab.cern.ch/alike/ali-docker/raw/master/scripts/ali-docker -O
chmod +x ali-docker
```
Run script
```
[mvala@vala tmp]$ ./ali-docker run home gui
Running docker image 'ali:latest' ...
sudo docker run -it --privileged -ti -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v /cvmfs:/cvmfs:shared -e TZ=Europe/Bratislava -v /home/mvala:/home/user  -e DISPLAY=:0 -v /tmp/.X11-unix:/tmp/.X11-unix --user user -w /home/user  docker.io/mvala/ali:latest /bin/bash
Unable to find image 'docker.io/mvala/ali:latest' locally
Trying to pull repository docker.io/mvala/ali ... 
sha256:2bfce973fa1f6538d39d65b3494700030f324ec8d72bf5b69ec806e23d3022cd: Pulling from docker.io/mvala/ali
5e35d10a3eba: Pull complete 
643c8849ddb6: Pull complete 
48ca6d74c625: Pull complete 
405f2627838e: Pull complete 
8f2990bead56: Pull complete 
c840a19192f8: Pull complete 
3dc4169cf34a: Pull complete 
579f46e5b18c: Pull complete 
951c518a7e34: Pull complete 
a922e09893e6: Pull complete 
3a008ad7fb59: Pull complete 
3b8840ac62c1: Pull complete 
5ea5d23f8667: Pull complete 
2971086b8d8f: Pull complete 
42113c2819f7: Pull complete 
9c2568318207: Pull complete 
Digest: sha256:2bfce973fa1f6538d39d65b3494700030f324ec8d72bf5b69ec806e23d3022cd
Status: Downloaded newer image for docker.io/mvala/ali:latest
[user@6583193d2f96 ~]$ exit
```
### OSX and Docker
In order tu run docker on mac you have to download the docker first, e.g. from the web page:

https://store.docker.com/editions/community/docker-ce-desktop-mac

Make sure you have installed and set cvmfs if you want to run root and aliroot with docker on mac. 
The settings for mac are a bit trickier because of the gui. To be able to use X11, you need to have installed XQuartz.
You can get a .dmg file it at:

https://www.xquartz.org

Then you need to install socat application, for example like this:

```
brew install socat
```
Now you can type series of commands:
```
open -a XQuartz
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
xhost
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip
```
To start docker with proper display you type:

```
sudo docker run -it --privileged -ti -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v /cvmfs:/cvmfs -e TZ=Europe/Bratislava -v /Users/mbombara:/home/user  -e DISPLAY=$ip:0 -v /tmp/.X11-unix:/tmp/.X11-unix --user user -w /home/user  docker.io/mvala/ali:latest /bin/bash
```
Now you should be inside docker with proper display. To load root and aliroot within the docker just type:
```
source /cvmfs/alice.cern.ch/etc/login.sh && alienv setenv VO_ALICE@AliPhysics,CMake/v2.8.12-2 -c /bin/bash
```




